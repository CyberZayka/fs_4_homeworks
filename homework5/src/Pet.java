import java.util.Arrays;

public class Pet {
    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public Pet(Species species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet(Species species, String nickname) {
        this(species, nickname, 0, 0, new String[]{});
    }
    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public void eat() {
        System.out.println("Я їм");
    }
    public void respond() {
        System.out.printf("Привіт, господар! Я - %s. Я скучив.\n", this.nickname);
    }
    public void foul() {
        System.out.println("Треба замісти сліди...");
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(species.name());
        sb.append("{");
        sb.append("nickname='").append(nickname).append("' ");
        if(age != 0) {
            sb.append(", age=").append(age);
        }
        if(trickLevel != 0) {
            sb.append(", trickLevel=").append(trickLevel);
        }
        if(habits.length != 0) {
            sb.append(", habits=").append(Arrays.toString(habits));
        }
        sb.append("}");
        return sb.toString();
    }
    @Override
    protected void finalize() throws Throwable {
        try {
            System.out.printf("Instance %s will be deleted\n", this);
        } catch (Throwable ex) {
            throw ex;
        } finally {
            super.finalize();
            System.out.println("Instance was removed successfully\n");
        }
    }
}
