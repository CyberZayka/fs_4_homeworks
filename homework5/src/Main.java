public class Main {
    public static void main(String[] args) {
        for (int i = 0; i < 1000000; i++) {
            if(i % 2 == 0) {
                new Human("Віто", "Карлеоне", 1975, 120, new String[][]{{DayOfWeek.MONDAY.name(), "робота; погратись з дітьми"}, {DayOfWeek.TUESDAY.name(), "поїхати на СТО"}}, null);
            } else {
                new Human("Джейн", "Карлеоне", 1970, 0, new String[][]{{DayOfWeek.WEDNESDAY.name(), "робота; приготувати поїсти"}, {DayOfWeek.THURSDAY.name(), "погуляти з друзями"}}, null);
            }
        }
        Pet dog = new Pet(Species.DOG, "Роккі", 5, 75, new String[]{"їсти", "пити", "спати"});
        System.out.println(dog);
        Pet cat = new Pet(Species.CAT, "Барсік", 3, 43, new String[]{"їсти", "пити", "спати"});
        System.out.println(cat);
        Pet raccoon = new Pet(Species.RACCOON, "Ракета");
        System.out.println(raccoon);
        Human man = new Human("Віто", "Карлеоне", 1975, 0, new String[][]{{DayOfWeek.SATURDAY.name(), "поїхати на рибалку; відпочивати"}, {DayOfWeek.SUNDAY.name(), "проходити курси"}}, null);
        System.out.println(man);
    }
}