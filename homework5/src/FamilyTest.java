import org.junit.Test;
import static org.junit.Assert.*;

public class FamilyTest {
    @Test
    public void testToString() {
        Human mother = new Human("Jane", "Doe", 1970, 0);
        Human father = new Human("John", "Doe", 1970, 0);
        Human child = new Human("Junior", "Doe", 2000, 0);

        Family family = new Family(mother, father, new Human[]{child});

        String test = "Family{mother='Human{name='Jane' surname='Doe' year='1970' }' father='Human{name='John' surname='Doe' year='1970' }' children='[Human{name='Junior' surname='Doe' year='2000' }]' }";
        String result = family.toString();

        assertEquals(test, result);
    }

    @Test
    public void testDeleteChild() {
        Human mother = new Human("Jane", "Doe", 1970, 0);
        Human father = new Human("John", "Doe", 1970, 0);
        Human child = new Human("Junior", "Doe", 2000, 0);

        Family family = new Family(mother, father, new Human[]{child});

        assertEquals(3, family.countFamily());

        boolean checkDeleted = family.deleteChild(0);

        assertEquals(2, family.countFamily());
        assertTrue(checkDeleted);

        try {
            boolean checkDeletedAnother = family.deleteChild(100);

            fail("index must be less than length");
            assertEquals(2, family.countFamily());
            assertFalse(checkDeletedAnother);
        } catch (IllegalStateException ex) {
            assertEquals("index must be less than length", ex.getMessage());
        }
    }

    @Test
    public void testAddChild() {
        Human mother = new Human("Jane", "Doe", 1970, 0);
        Human father = new Human("John", "Doe", 1970, 0);
        Human child1 = new Human("Junior", "Doe", 2000, 0);

        Family family = new Family(mother, father, new Human[]{child1});

        assertEquals(3, family.countFamily());

        Human child2 = new Human("Brad", "Pitt", 1970, 0);
        family.addChild(child2);

        assertEquals(4, family.countFamily());
        assertEquals(child2.hashCode(), family.getChildren()[1].hashCode());
    }

    @Test
    public void testCountFamily() {
        Human mother = new Human("Jane", "Doe", 1970, 0);
        Human father = new Human("John", "Doe", 1970, 0);
        Human child = new Human("Junior", "Doe", 2000, 0);

        Family family = new Family(mother, father, new Human[]{child});

        assertEquals(3, family.countFamily());
    }
}