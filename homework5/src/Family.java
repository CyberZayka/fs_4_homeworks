import java.util.Arrays;

public class Family {
    private final Human mother;
    private final Human father;
    private Human[] children;
    Family(
            Human mother,
            Human father,
            Human[] children
    ) {
        this.mother = mother;
        this.father = father;
        this.children = children;
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void addChild(Human child) {
        Human[] newChildren = new Human[children.length + 1];
        System.arraycopy(
                children, 0,
                newChildren, 0,
                children.length
        );
        newChildren[newChildren.length - 1] = child;
        children = newChildren;
    }
    public boolean deleteChild(int index) {
        if (index < 0) throw new IllegalStateException("index must be positive");
        if (index > children.length) throw new IllegalStateException("index must be less than length");

        Human[] newChildren = new Human[children.length - 1];
        int j = 0;
        boolean deleted = false;
        for (int i = 0; i < children.length; i++) {
            if (i != index) {
                newChildren[j] = children[i];
                j++;
            } else {
                deleted = true;
            }
        }
        if (deleted) {
            children = newChildren;
            return true;
        } else {
            return false;
        }
    }

    public int countFamily() {
        return children.length + 2;
    }

    @Override
    public String toString() {
        return "Family" + "{" +
                "mother='" + mother + "' " +
                "father='" + father + "' " +
                "children='" + Arrays.toString(children) + "' " +
                "}";
    }
    @Override
    protected void finalize() throws Throwable {
        try {
            System.out.printf("Instance %s will be deleted\n", this);
        } catch (Throwable ex) {
            throw ex;
        } finally {
            super.finalize();
            System.out.println("Instance was removed successfully\n");
        }
    }
}
