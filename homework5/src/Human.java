import java.util.Arrays;
import java.util.Objects;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private String[][] schedule;
    private Pet pet;

    public Human(String name, String surname, int year, int iq, String[][] schedule, Pet pet) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
        this.pet = pet;
    }
    public Human(String name, String surname, int year, int iq) {
        this(name, surname, year, iq, new String[][]{}, null);
    }
    public Human(String name, String surname, int year, Pet pet) {
        this(name, surname, year, 0, new String[][]{}, pet);
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
    public String getFullName() { return name + " " + surname; }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Pet getPet() {
        return pet;
    }
    public void greetPet() {
        System.out.printf("Привіт, %s \n", this.pet.getNickname());
    }
    public void describePet() {
        String trick = this.pet.getTrickLevel() <= 50 ? "майже не хитрий" : "дуже хитрий";

        System.out.printf("У мене є %s, йому %s років, він %s.\n", this.pet.getSpecies(), this.pet.getAge(), trick);
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (this == other) return true;

        if (!(
                other.getClass().equals(this.getClass())
        )) return false;
        Human that = (Human) other;

        return Objects.equals(this.name, that.name) &&
                Objects.equals(this.surname, that.surname) &&
                Objects.equals(this.year, that.year);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Human");
        sb.append("{");
        sb.append("name='").append(name).append("' ");
        sb.append("surname='").append(surname).append("' ");
        sb.append("year='").append(year).append("' ");
        if(iq != 0) {
            sb.append("iq='").append(iq).append("' ");
        }
        if(schedule.length != 0) {
            sb.append("schedule='").append(Arrays.deepToString(schedule)).append("' ");
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            System.out.printf("Instance %s will be deleted\n", this);
        } catch (Throwable ex) {
            throw ex;
        } finally {
            super.finalize();
            System.out.println("Instance was removed successfully\n");
        }
    }
}