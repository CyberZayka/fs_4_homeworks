import family_project.DayOfWeek;
import family_project.humans.Child;
import family_project.humans.Family;
import family_project.humans.Man;
import family_project.humans.Woman;
import org.junit.Test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import static org.junit.Assert.*;

public class FamilyTest {
    @Test
    public void testToString() throws ParseException {
        HashMap<String, String> manSchedule = new HashMap<String, String>();
        manSchedule.put(DayOfWeek.MONDAY.name(), "поїхати на СТО");
        manSchedule.put(DayOfWeek.THURSDAY.name(), "курси");

        Man father = new Man("John", "Doe", "20/03/1970", 120, manSchedule, new HashSet<>());
        Woman mother = new Woman("Jane", "Doe", "15/04/1970", 120, new HashMap<String, String>(), new HashSet<>());
        Child child = new Child("Junior", "Doe", "20/03/2016", new HashSet<>());

        ArrayList<Child> childrenList = new ArrayList<Child>();
        childrenList.add(child);

        Family family = new Family(mother.getFullName(), father.getFullName(), childrenList);

        String test = "Family{mother='Jane Doe' father='John Doe' children='[Human{name='Junior' surname='Doe' birthDate='19/03/2016' }]' }";
        String result = family.toString();

        assertEquals(test, result);
    }

    @Test
    public void testDeleteChild() throws ParseException {
        HashMap<String, String> manSchedule = new HashMap<String, String>();
        manSchedule.put(DayOfWeek.MONDAY.name(), "поїхати на СТО");
        manSchedule.put(DayOfWeek.THURSDAY.name(), "курси");

        Man father = new Man("John", "Doe", "20/03/1970", 120, manSchedule, new HashSet<>());
        Woman mother = new Woman("Jane", "Doe", "15/04/1970", 120, new HashMap<String, String>(), new HashSet<>());
        Child child = new Child("Junior", "Doe", "20/03/2016", new HashSet<>());

        ArrayList<Child> childrenList = new ArrayList<Child>();
        childrenList.add(child);

        Family family = new Family(mother.getFullName(), father.getFullName(), childrenList);

        assertEquals(3, family.countFamily());

        boolean checkDeleted = family.deleteChild(0);

        assertEquals(2, family.countFamily());
        assertTrue(checkDeleted);

        try {
            boolean checkDeletedAnother = family.deleteChild(100);

            fail("index must be less than length");
            assertEquals(2, family.countFamily());
            assertFalse(checkDeletedAnother);
        } catch (IllegalStateException ex) {
            assertEquals("index must be less than length", ex.getMessage());
        }
    }

    @Test
    public void testAddChild() throws ParseException {
        HashMap<String, String> manSchedule = new HashMap<String, String>();
        manSchedule.put(DayOfWeek.MONDAY.name(), "поїхати на СТО");
        manSchedule.put(DayOfWeek.THURSDAY.name(), "курси");

        Man father = new Man("John", "Doe", "20/03/1970", 120, manSchedule, new HashSet<>());
        Woman mother = new Woman("Jane", "Doe", "15/04/1970", 120, new HashMap<String, String>(), new HashSet<>());
        Child child = new Child("Junior", "Doe", "20/03/2016", new HashSet<>());

        ArrayList<Child> childrenList = new ArrayList<Child>();
        childrenList.add(child);

        Family family = new Family(mother.getFullName(), father.getFullName(), childrenList);

        assertEquals(3, family.countFamily());

        Child child2 = new Child("Brad", "Pitt", "20/03/2016", new HashSet<>());
        family.addChild(child2);

        assertEquals(4, family.countFamily());
        assertEquals(child2.hashCode(), family.getChildren().get(1).hashCode());
    }

    @Test
    public void testCountFamily() throws ParseException {
        HashMap<String, String> manSchedule = new HashMap<String, String>();
        manSchedule.put(DayOfWeek.MONDAY.name(), "поїхати на СТО");
        manSchedule.put(DayOfWeek.THURSDAY.name(), "курси");

        Man father = new Man("John", "Doe", "20/03/1970", 120, manSchedule, new HashSet<>());
        Woman mother = new Woman("Jane", "Doe", "15/04/1970", 120, new HashMap<String, String>(), new HashSet<>());
        Child child = new Child("Junior", "Doe", "20/03/2016", new HashSet<>());

        ArrayList<Child> childrenList = new ArrayList<Child>();
        childrenList.add(child);

        Family family = new Family(mother.getFullName(), father.getFullName(), childrenList);

        assertEquals(3, family.countFamily());
    }
}