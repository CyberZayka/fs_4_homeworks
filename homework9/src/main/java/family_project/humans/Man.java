package family_project.humans;

import family_project.animals.Pet;

import java.text.ParseException;
import java.util.HashMap;
import java.util.HashSet;

final public class Man extends Human {
    public Man(String name, String surname, String birthDate, int iq, HashMap<String, String> schedule, HashSet<Pet> pet) throws ParseException {
        super(name, surname, birthDate, iq, schedule, pet);
    }

    public void repairCar() {
        System.out.println("Машина заглохла, треба відремонтувати!");
    }

    @Override
    public void greetPet() {
        for (Pet petElement : super.getPet()) {
            if (super.getPet().size() == 1) {
                System.out.printf("Привіт, %s, пішли гуляти!\n", petElement.getNickname());
            } else if (super.getPet().size() > 1){
                System.out.println("Привіт, тварини, пішли гуляти");
            }
        }
    }
}
