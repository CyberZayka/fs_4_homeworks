package family_project.humans;

import family_project.animals.Pet;

import java.text.ParseException;
import java.util.HashMap;
import java.util.HashSet;

final public class Child extends Human {
    public Child(String name, String surname, String birthDate, HashSet<Pet> pet) throws ParseException {
        super(name, surname, birthDate, pet);
    }
    public Child(String name, String surname, String birthDate, int iq) throws ParseException {
        super(name, surname, birthDate, iq, new HashSet<>());
    }
    @Override
    public void greetPet() {
        for (Pet petElement : super.getPet()) {
            if (super.getPet().size() == 1) {
                System.out.printf("Пішли гратися, %s ?\n", petElement.getNickname());
            } else if (super.getPet().size() > 1){
                System.out.println("Хто хоче погратися?");
            }
        }
    }
}
