package family_project.humans;

import family_project.animals.Pet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.*;

abstract class Human {
    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private HashMap<String, String> schedule;
    private HashSet<Pet> pet;

    public Human(String name, String surname, String birthDate, int iq, HashMap<String, String> schedule, HashSet<Pet> pet) throws ParseException {
        this.name = name;
        this.surname = surname;
        this.birthDate = dateParser(birthDate);
        this.iq = iq;
        this.schedule = schedule;
        this.pet = pet;
    }
    public Human(String name, String surname, String birthDate, int iq, HashSet<Pet> pet) throws ParseException {
        this(name, surname, birthDate, iq, new HashMap<String, String>(), pet);
    }
    public Human(String name, String surname, String birthDate, HashSet<Pet> pet) throws ParseException {
        this(name, surname, birthDate, 0, new HashMap<String, String>(), pet);
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
    public String getFullName() { return name + " " + surname; }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }
    public HashSet<Pet> getPet() {
        return pet;
    }
    abstract public void greetPet();
    public void describePet() {

        for (Pet petElement : pet) {
            String trick = petElement.getTrickLevel() <= 50 ? "майже не хитрий" : "дуже хитрий";

            System.out.printf("У мене є %s, йому %s років, він %s.\n", petElement.getSpecies(), petElement.getAge(), trick);
        }

    }

    public long dateParser(String dateString) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = dateFormat.parse(dateString);
            return date.getTime();
        } catch (ParseException ex) {
            System.out.println(ex);
        }
        return 0;
    }

    public String timestampToDateConverter(long birthMilli) {
        LocalDate date = LocalDate.ofInstant(Instant.ofEpochMilli(birthMilli), ZoneId.of("UTC"));

        DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                .appendValue(ChronoField.DAY_OF_MONTH, 2)
                .appendLiteral("/")
                .appendValue(ChronoField.MONTH_OF_YEAR, 2)
                .appendLiteral("/")
                .appendValue(ChronoField.YEAR, 4)
                .toFormatter();

        return date.format(formatter);
    }

    public String describeAge() {
        long currentTimestampMillis = System.currentTimeMillis();

        LocalDate birth = LocalDate.ofInstant(Instant.ofEpochMilli(birthDate), ZoneId.of("UTC"));
        LocalDate currentDate = LocalDate.ofInstant(Instant.ofEpochMilli(currentTimestampMillis), ZoneId.of("UTC"));

        Period age = Period.between(birth, currentDate);
        int years = age.getYears();
        int months = age.getMonths();
        int days = age.getDays();

        System.out.printf("Age: %d years, %d months, %d days\n", years, months, days);
        return String.format("Age: %d years, %d months, %d days", years, months, days);
    }
    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (this == other) return true;

        if (!(
                other.getClass().equals(this.getClass())
        )) return false;
        Human that = (Human) other;

        return Objects.equals(this.name, that.name) &&
                Objects.equals(this.surname, that.surname);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Human");
        sb.append("{");
        sb.append("name='").append(name).append("' ");
        sb.append("surname='").append(surname).append("' ");
        sb.append("birthDate='").append(timestampToDateConverter(birthDate)).append("' ");
        if(iq != 0) {
            sb.append("iq='").append(iq).append("' ");
        }
        if(schedule.size() != 0) {
            sb.append("schedule='").append(schedule).append("' ");
        }
        if(pet.size() != 0) {
            sb.append("pet='").append(pet).append("' ");
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            System.out.printf("Instance %s will be deleted\n", this);
        } catch (Throwable ex) {
            throw ex;
        } finally {
            super.finalize();
            System.out.println("Instance was removed successfully\n");
        }
    }
}