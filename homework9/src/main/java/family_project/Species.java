package family_project;

public enum Species {
    DOG,
    DOMESTICCAT,
    ROBOCAT,
    FISH,
    UNKNOWN,
}
