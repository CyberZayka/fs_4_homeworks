public class Main {
    public static void main(String[] args) {
        Pet pet = new Pet(Species.DOG, "Роккі", 5, 75, new String[]{"їсти", "пити", "спати"});
        Human mother = new Human("Джейн", "Карлеоне", 1977, 120);
        Human father = new Human("Віто", "Карлеоне", 1975, 0, new String[][]{{"понеділок", "робота; погратись з дітьми"}, {"вівторок", "поїхати на СТО"}}, pet);
        Human Michael = new Human("Майкл", "Карлеоне", 2001, 0);
        Human John = new Human("Джон", "Доу", 2005, 0);
        Family family = new Family(mother, father, new Human[]{Michael, John});

        System.out.println("-------------------------");
        System.out.println("Properties of the pet are below:");
        System.out.println("-------------------------");
        System.out.println(pet);
        pet.eat();
        pet.respond();
        pet.foul();

        System.out.println("-------------------------");
        System.out.println("Properties of mother are below:");
        System.out.println("-------------------------");
        System.out.println(mother);

        System.out.println("-------------------------");
        System.out.println("Properties of father are below:");
        System.out.println("-------------------------");
        System.out.println(father);
        father.greetPet();
        father.describePet();

        System.out.println("-------------------------");
        System.out.println("Properties of Michael are below:");
        System.out.println("-------------------------");
        System.out.println(Michael);

        System.out.println("-------------------------");
        System.out.println("Properties of John are below:");
        System.out.println("-------------------------");
        System.out.println(John);

        System.out.println("-------------------------");
        System.out.println("Properties of whole family:");
        System.out.println("-------------------------");
        System.out.println(family);
        System.out.println(family.countFamily());
        System.out.println(family.deleteChild(1));
        System.out.println(family.countFamily());
        family.addChild(new Human("Bred", "Pitt", 1965, 0));
        System.out.println(family.countFamily());
        System.out.println(family);
    }
}