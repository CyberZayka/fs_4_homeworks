package family_project.animals;

import family_project.Species;

import java.util.HashSet;

public class Fish extends Pet {
    public Fish(String nickname, int age) {
        super(nickname, age, 0, new HashSet<String>());
        super.setSpecies(Species.FISH);
    }

    @Override
    public void respond() {
        System.out.println("буль-буль...");
    }
}
