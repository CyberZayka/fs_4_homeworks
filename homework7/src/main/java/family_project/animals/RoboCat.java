package family_project.animals;

import family_project.Species;

import java.util.HashSet;

public class RoboCat extends Pet implements IPet {
    private final int desireToConquerHumanityIndex;
    public RoboCat(String nickname, int age, int trickLevel, HashSet<String> habits, int desireToConquerHumanity) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.ROBOCAT);
        this.desireToConquerHumanityIndex = desireToConquerHumanity;
    }

    boolean desireToConquerHumanityCheck(int idx) {
        return idx >= 50 && idx <= 100;
    }

    @Override
    public void respond() {
        System.out.println("Привіт, шкіряний мішок");
    }
    @Override
    public void foul() {
        if (desireToConquerHumanityCheck(desireToConquerHumanityIndex)) {
            System.out.println("Коти-роботи будуть правити світом");
        } else {
            System.out.println("Прибери за мною, шкіряний мішок");
        }
    }
}
