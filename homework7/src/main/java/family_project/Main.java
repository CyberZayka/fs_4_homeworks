package family_project;

import family_project.animals.*;
import family_project.humans.Child;
import family_project.humans.Man;
import family_project.humans.Woman;

import java.util.HashMap;
import java.util.HashSet;

public class Main {
    public static void main(String[] args) {
        HashSet<String> dogHabbits = new HashSet<String>();
        dogHabbits.add("їсти");
        dogHabbits.add("охороняти");

        HashSet<String> dmCatHabbits = new HashSet<String>();
        dmCatHabbits.add("їсти");
        dmCatHabbits.add("спати");

        HashSet<String> rbCatHabbits = new HashSet<String>();
        rbCatHabbits.add("їсти");
        rbCatHabbits.add("спати");
        rbCatHabbits.add("покоряти людство");

        HashMap<String, String> manSchedule = new HashMap<String, String>();
        manSchedule.put(DayOfWeek.MONDAY.name(), "поїхати на СТО");
        manSchedule.put(DayOfWeek.THURSDAY.name(), "курси");

        Dog dog = new Dog("Роккі", 5, dogHabbits);
        Fish fish = new Fish("Гуппі", 1);
        DomesticCat dmCat = new DomesticCat("Барсік", 2, 33, dmCatHabbits);
        RoboCat rbCat = new RoboCat("Робокіт", 3, 75, rbCatHabbits, 66);
        Tiranosaur tRex = new Tiranosaur("Рекс", 65000000);

        HashSet<Pet> pets = new HashSet<Pet>();
        pets.add(dog);
        pets.add(fish);

        Man man = new Man("John", "Doe", 1970, 120, manSchedule, pets);
        Woman woman = new Woman("Jane", "Doe", 1970, 120, new HashMap<String, String>(), new HashSet<>());
        Child child = new Child("Junior", "Doe", 2000, new HashSet<>());

        System.out.println("-----------------------");
        System.out.println("Below information about a dog");

        System.out.println(dog);
        dog.respond();
        dog.eat();

        System.out.println("-----------------------");
        System.out.println("Below information about a fish");

        System.out.println(fish);
        fish.respond();
        fish.eat();

        System.out.println("-----------------------");
        System.out.println("Below information about a domestic cat");

        System.out.println(dmCat);
        dmCat.respond();
        dmCat.eat();
        dmCat.foul();

        System.out.println("-----------------------");
        System.out.println("Below information about a robo-cat");

        System.out.println(rbCat);
        rbCat.respond();
        rbCat.eat();
        rbCat.foul();

        System.out.println("-----------------------");
        System.out.println("Below information about a tiranosaur");

        System.out.println(tRex);
        tRex.respond();
        tRex.eat();

        System.out.println("-----------------------");
        System.out.println("Below information about a man");

        System.out.println(man);
        man.greetPet();
        man.describePet();
        man.repairCar();

        System.out.println("-----------------------");
        System.out.println("Below information about a woman");

        System.out.println(woman);
        woman.greetPet();
        woman.describePet();
        woman.makeup();

        System.out.println("-----------------------");
        System.out.println("Below information about a child");

        System.out.println(child);
        child.greetPet();
        child.describePet();
    }
}