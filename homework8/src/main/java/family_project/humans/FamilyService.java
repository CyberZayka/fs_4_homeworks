package family_project.humans;

import family_project.animals.Pet;

import java.util.List;

public class FamilyService implements FamilyDao {
    private CollectionFamilyDao FamilyDao;

    @Override
    public List getAllFamilies() {
        return null;
    }

    @Override
    public void displayAllFamilies() {

    }

    @Override
    public List<Family> getFamiliesBiggerThan(int amount) {
        return null;
    }

    @Override
    public List<Family> getFamiliesLessThan(int amount) {
        return null;
    }

    @Override
    public int countFamiliesWithMemberNumber(int amount) {
        return 0;
    }

    @Override
    public void createNewFamily(Human mother, Human father) {

    }

    @Override
    public Family bornChild(Family family, String womanName, String manName) {
        return null;
    }

    @Override
    public Family adoptChild(Family family, Human child) {
        return null;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return null;
    }

    @Override
    public boolean deleteFamilyByIndex(int index) {
        return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        return false;
    }

    @Override
    public void deleteAllChildrenOlderThan(int age) {

    }

    @Override
    public int count() {
        return 0;
    }

    @Override
    public void saveFamily(Family family) {

    }

    @Override
    public List<Pet> getPets(int index) {
        return null;
    }

    @Override
    public void addPet(int index) {

    }
}
