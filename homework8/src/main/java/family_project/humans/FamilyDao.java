package family_project.humans;

import family_project.animals.Pet;

import java.util.List;

public interface FamilyDao {
    List<Family> getAllFamilies();
    void displayAllFamilies();
    List<Family> getFamiliesBiggerThan(int amount);
    List<Family> getFamiliesLessThan(int amount);
    int countFamiliesWithMemberNumber(int amount);
    void createNewFamily(Human mother, Human father);
    Family bornChild(Family family, String womanName, String manName);
    Family adoptChild(Family family, Human child);
    Family getFamilyByIndex(int index);
    boolean deleteFamilyByIndex(int index);
    boolean deleteFamily(Family family);
    void deleteAllChildrenOlderThan(int age);
    int count();
    void saveFamily(Family family);
    List<Pet> getPets(int index);
    void addPet(int index);
}
