package family_project.humans;

import family_project.animals.Pet;

import java.util.HashMap;
import java.util.HashSet;

final public class Child extends Human {
    public Child(String name, String surname, int year, HashSet<Pet> pet) {
        super(name, surname, year, 0, new HashMap<String, String>(), pet);
    }
    @Override
    public void greetPet() {
        for (Pet petElement : super.getPet()) {
            if (super.getPet().size() == 1) {
                System.out.printf("Пішли гратися, %s ?\n", petElement.getNickname());
            } else if (super.getPet().size() > 1){
                System.out.println("Хто хоче погратися?");
            }
        }
    }
}
