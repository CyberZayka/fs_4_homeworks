import family_project.humans.Child;
import family_project.humans.Family;
import family_project.humans.Man;
import family_project.humans.Woman;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import static org.junit.Assert.*;

import family_project.*;

public class FamilyTest {
    @Test
    public void testToString() {
        HashMap<String, String> manSchedule = new HashMap<String, String>();
        manSchedule.put(DayOfWeek.MONDAY.name(), "поїхати на СТО");
        manSchedule.put(DayOfWeek.THURSDAY.name(), "курси");

        Man father = new Man("John", "Doe", 1970, 120, manSchedule, new HashSet<>());
        Woman mother = new Woman("Jane", "Doe", 1970, 120, new HashMap<String, String>(), new HashSet<>());
        Child child = new Child("Junior", "Doe", 2000, new HashSet<>());

        ArrayList<Child> childrenList = new ArrayList<Child>();
        childrenList.add(child);

        Family family = new Family(mother.getFullName(), father.getFullName(), childrenList);

        String test = "Family{mother='Jane Doe' father='John Doe' children='[Human{name='Junior' surname='Doe' year='2000' }]' }";
        String result = family.toString();

        assertEquals(test, result);
    }

    @Test
    public void testDeleteChild() {
        HashMap<String, String> manSchedule = new HashMap<String, String>();
        manSchedule.put(DayOfWeek.MONDAY.name(), "поїхати на СТО");
        manSchedule.put(DayOfWeek.THURSDAY.name(), "курси");

        Man father = new Man("John", "Doe", 1970, 120, manSchedule, new HashSet<>());
        Woman mother = new Woman("Jane", "Doe", 1970, 120, new HashMap<String, String>(), new HashSet<>());
        Child child = new Child("Junior", "Doe", 2000, new HashSet<>());

        ArrayList<Child> childrenList = new ArrayList<Child>();
        childrenList.add(child);

        Family family = new Family(mother.getFullName(), father.getFullName(), childrenList);

        assertEquals(3, family.countFamily());

        boolean checkDeleted = family.deleteChild(0);

        assertEquals(2, family.countFamily());
        assertTrue(checkDeleted);

        try {
            boolean checkDeletedAnother = family.deleteChild(100);

            fail("index must be less than length");
            assertEquals(2, family.countFamily());
            assertFalse(checkDeletedAnother);
        } catch (IllegalStateException ex) {
            assertEquals("index must be less than length", ex.getMessage());
        }
    }

    @Test
    public void testAddChild() {
        HashMap<String, String> manSchedule = new HashMap<String, String>();
        manSchedule.put(DayOfWeek.MONDAY.name(), "поїхати на СТО");
        manSchedule.put(DayOfWeek.THURSDAY.name(), "курси");

        Man father = new Man("John", "Doe", 1970, 120, manSchedule, new HashSet<>());
        Woman mother = new Woman("Jane", "Doe", 1970, 120, new HashMap<String, String>(), new HashSet<>());
        Child child = new Child("Junior", "Doe", 2000, new HashSet<>());

        ArrayList<Child> childrenList = new ArrayList<Child>();
        childrenList.add(child);

        Family family = new Family(mother.getFullName(), father.getFullName(), childrenList);

        assertEquals(3, family.countFamily());

        Child child2 = new Child("Brad", "Pitt", 1970, new HashSet<>());
        family.addChild(child2);

        assertEquals(4, family.countFamily());
        assertEquals(child2.hashCode(), family.getChildren().get(1).hashCode());
    }

    @Test
    public void testCountFamily() {
        HashMap<String, String> manSchedule = new HashMap<String, String>();
        manSchedule.put(DayOfWeek.MONDAY.name(), "поїхати на СТО");
        manSchedule.put(DayOfWeek.THURSDAY.name(), "курси");

        Man father = new Man("John", "Doe", 1970, 120, manSchedule, new HashSet<>());
        Woman mother = new Woman("Jane", "Doe", 1970, 120, new HashMap<String, String>(), new HashSet<>());
        Child child = new Child("Junior", "Doe", 2000, new HashSet<>());

        ArrayList<Child> childrenList = new ArrayList<Child>();
        childrenList.add(child);

        Family family = new Family(mother.getFullName(), father.getFullName(), childrenList);

        assertEquals(3, family.countFamily());
    }
}