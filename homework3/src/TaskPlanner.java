import java.util.Objects;
import java.util.Scanner;

public class TaskPlanner {
    public static void main(String[] args) {
        String[][] schedule = new String[7][2];

        fillSchedule(schedule);

        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Please, input the day of the week:");

            String line = scanner
                    .nextLine()
                    .trim()
                    .toLowerCase();

            if (Objects.equals(line, "exit")) {
                break;
            }

            switch (line) {
                case "sunday" -> {
                    System.out.printf("Your tasks for sunday: %s ", schedule[0][1]);
                    System.out.println();
                }
                case "monday" -> {
                    System.out.printf("Your tasks for monday: %s ", schedule[1][1]);
                    System.out.println();
                }
                case "tuesday" -> {
                    System.out.printf("Your tasks for tuesday: %s ", schedule[2][1]);
                    System.out.println();
                }
                case "wednesday" -> {
                    System.out.printf("Your tasks for wednesday: %s ", schedule[3][1]);
                    System.out.println();
                }
                case "thursday" -> {
                    System.out.printf("Your tasks for thursday: %s ", schedule[4][1]);
                    System.out.println();
                }
                case "friday" -> {
                    System.out.printf("Your tasks for friday: %s ", schedule[5][1]);
                    System.out.println();
                }
                case "saturday" -> {
                    System.out.printf("Your tasks for saturday: %s ", schedule[6][1]);
                    System.out.println();
                }
                default -> System.out.println("Sorry, I don't understand you, please try again.");
            }
        }
    }

    public static void fillSchedule(String[][] schedule) {
        schedule[0][0] = "Sunday";
        schedule[0][1] = "do home work";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "working on the new project";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "continue work on the new project";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "continue work on the new project";
        schedule[5][0] = "Friday";
        schedule[5][1] = "reading a book; get a rest";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "no plans for today";
    }
}