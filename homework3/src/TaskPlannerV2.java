import java.util.Scanner;

public class TaskPlannerV2 {
    public static void main(String[] args) {
        String[] tasks = {
                "do home work",
                "go to courses; watch a film",
                "working on the new project",
                "continue work on the new project",
                "continue work on the new project",
                "reading a book; get a rest",
                "no plans for today"
        };

        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Please, input the day of the week:");

            String line = scanner
                    .nextLine()
                    .trim()
                    .toLowerCase();

            if (line.equals("exit")) {
                break;
            }

            try {
                DayOfWeek dayOfWeek = DayOfWeek.valueOf(line.toUpperCase());
                int index = dayOfWeek.ordinal();

                if (index >= 0 && index < tasks.length) {
                    String task = tasks[index];
                    System.out.printf("Your tasks for %s: %s%n", dayOfWeek.name(), task);
                } else {
                    System.out.println("Sorry, I don't understand you, please try again.");
                }
            } catch (IllegalArgumentException ex) {
                System.out.println("Sorry, I don't understand you, please try again.");
            }
        }
    }

}
