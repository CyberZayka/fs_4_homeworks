import java.util.Scanner;

public class AreaShooting {
    public static void main(String[] args) {
        int[][] board = new int[5][5];

        int targetRow = (int) (Math.random() * 5);
        int targetCol = (int) (Math.random() * 5);

        System.out.println("All set. Get ready to rumble!");

        Scanner scanner = new Scanner(System.in);

        while (true) {
            for (int row = 0; row < board.length; row++) {
                System.out.print("|");
                for (int col = 0; col < board[row].length; col++) {
                    System.out.printf(" %s |", representCell(board[row][col]));
                }
                System.out.println();
            }

            System.out.print("Enter row number (1-5): ");
            int row = getValidInput(scanner);
            System.out.print("Enter column number (1-5): ");
            int col = getValidInput(scanner);

            if (row - 1 == targetRow && col - 1 == targetCol) {
                System.out.println("You have won!");
                board[targetRow][targetCol] = 2;
                break;
            } else {
                System.out.println("Missed!");
                board[row - 1][col - 1] = 1;
            }
        }
    }

    public static int getValidInput(Scanner scanner) {
        while (!scanner.hasNextInt()) {
            System.out.print("Invalid input. Enter a number: ");
            scanner.next();
        }
        int input = scanner.nextInt();
        while (input < 1 || input > 5) {
            System.out.print("Invalid input. Enter a number between 1 and 5: ");
            input = scanner.nextInt();
        }
        return input;
    }

    public static String representCell(int value) {
        if (value == 0) return "-";
        if (value == 1) return "*";
        return "x";
    }
}