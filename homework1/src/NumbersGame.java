import java.util.Arrays;
import java.util.Scanner;
import java.util.Random;

public class NumbersGame {
    public static int getRandomNumber(int min, int max) {
        int diff = max - min;
        Random rand = new Random();
        int number = rand.nextInt(diff + 1);
        int result = number + min;
        return result;
    }

    public static void print(String str) {
        System.out.print(str);
        System.out.println();
    }

    public static String toString(int[] array) {
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < array.length; i++) {
            if (i > 0) sb.append(", ");
            sb.append(array[i]);
        }
        sb.append("]");
        return sb.toString();
    }

    public static void main(String[] args) {
        int min = 0;
        int max = 100;
        int number = getRandomNumber(min, max);
        int[] arrayNums = new int[max - min];
        int i = 0;

        print("Let the game begin!");
        Scanner scanner = new Scanner(System.in);
        print("Please enter your name: ");
        String name = scanner.nextLine();
        print("Welcome, " + name + "!");
        while (true) {
            print("Guess a number between 0 and 100: ");
            String line = scanner.nextLine();

            try {
                int guess = Integer.parseInt(line);
                arrayNums[i] = guess;
                i++;

                if (guess < number) {
                    print("Your number is too small. Please try again...");
                } else if (guess > number) {
                    print("Your number is too big. Please try again...");
                } else {
                    int[] filteredArray = new int[i];
                    System.arraycopy(arrayNums, 0, filteredArray, 0, i);
                    String resultArray = toString(filteredArray);
                    print("Congratulations, " + name + "!");
                    print("You have entered numbers such as: ");
                    print(resultArray);

                    break;
                }
            } catch (NumberFormatException ex) {
                print("You have entered not a number. Please, try again");
            }
        }
        scanner.close();
    }
}
