import family_project.humans.*;
import org.junit.Test;

import static org.junit.Assert.*;

import family_project.*;

public class FamilyTest {
    @Test
    public void testToString() {
        Man father = new Man("John", "Doe", 1970, 120, new String[][]{{DayOfWeek.MONDAY.name(), "поїхати на СТО"}, {DayOfWeek.THURSDAY.name(), "курси"}}, null);
        Woman mother = new Woman("Jane", "Doe", 1970, 120, new String[][]{}, null);
        Child child = new Child("Junior", "Doe", 2000, null);

        Family family = new Family(mother, father, new Human[]{child}, null);

        String test = "Family{mother='Human{name='Jane' surname='Doe' year='1970' iq='120' }' father='Human{name='John' surname='Doe' year='1970' iq='120' schedule='[[MONDAY, поїхати на СТО], [THURSDAY, курси]]' }' children='[Human{name='Junior' surname='Doe' year='2000' }]' }";
        String result = family.toString();

        assertEquals(test, result);
    }

    @Test
    public void testDeleteChild() {
        Man father = new Man("John", "Doe", 1970, 120, new String[][]{{DayOfWeek.MONDAY.name(), "поїхати на СТО"}, {DayOfWeek.THURSDAY.name(), "курси"}}, null);
        Woman mother = new Woman("Jane", "Doe", 1970, 120, new String[][]{}, null);
        Child child = new Child("Junior", "Doe", 2000, null);

        Family family = new Family(mother, father, new Human[]{child}, null);

        assertEquals(3, family.countFamily());

        boolean checkDeleted = family.deleteChild(0);

        assertEquals(2, family.countFamily());
        assertTrue(checkDeleted);

        try {
            boolean checkDeletedAnother = family.deleteChild(100);

            fail("index must be less than length");
            assertEquals(2, family.countFamily());
            assertFalse(checkDeletedAnother);
        } catch (IllegalStateException ex) {
            assertEquals("index must be less than length", ex.getMessage());
        }
    }

    @Test
    public void testAddChild() {
        Man father = new Man("John", "Doe", 1970, 120, new String[][]{{DayOfWeek.MONDAY.name(), "поїхати на СТО"}, {DayOfWeek.THURSDAY.name(), "курси"}}, null);
        Woman mother = new Woman("Jane", "Doe", 1970, 120, new String[][]{}, null);
        Child child = new Child("Junior", "Doe", 2000, null);

        Family family = new Family(mother, father, new Human[]{child}, null);

        assertEquals(3, family.countFamily());

        Human child2 = new Child("Brad", "Pitt", 1970, null);
        family.addChild(child2);

        assertEquals(4, family.countFamily());
        assertEquals(child2.hashCode(), family.getChildren()[1].hashCode());
    }

    @Test
    public void testCountFamily() {
        Man father = new Man("John", "Doe", 1970, 120, new String[][]{{DayOfWeek.MONDAY.name(), "поїхати на СТО"}, {DayOfWeek.THURSDAY.name(), "курси"}}, null);
        Woman mother = new Woman("Jane", "Doe", 1970, 120, new String[][]{}, null);
        Child child = new Child("Junior", "Doe", 2000, null);

        Family family = new Family(mother, father, new Human[]{child}, null);

        assertEquals(3, family.countFamily());
    }
}