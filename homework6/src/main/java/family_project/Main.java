package family_project;

import family_project.animals.Dog;
import family_project.animals.DomesticCat;
import family_project.animals.Fish;
import family_project.animals.RoboCat;
import family_project.animals.Tiranosaur;
import family_project.humans.Child;
import family_project.humans.Man;
import family_project.humans.Woman;

public class Main {
    public static void main(String[] args) {
        Dog dog = new Dog("Роккі", 5, new String[]{"їсти","охороняти"});
        Fish fish = new Fish("Гуппі", 1);
        DomesticCat dmCat = new DomesticCat("Барсік", 2, 33, new String[]{"їсти","спати"});
        RoboCat rbCat = new RoboCat("Робокіт", 3, 75, new String[]{"їсти","спати", "покоряти людство"}, 66);
        Tiranosaur tRex = new Tiranosaur("Рекс", 65000000);

        Man man = new Man("John", "Doe", 1970, 120, new String[][]{{DayOfWeek.MONDAY.name(), "поїхати на СТО"}, {DayOfWeek.THURSDAY.name(), "курси"}}, dog);
        Woman woman = new Woman("Jane", "Doe", 1970, 120, new String[][]{}, dmCat);
        Child child = new Child("Junior", "Doe", 2000, dmCat);

        System.out.println("-----------------------");
        System.out.println("Below information about a dog");

        System.out.println(dog);
        dog.respond();
        dog.eat();

        System.out.println("-----------------------");
        System.out.println("Below information about a fish");

        System.out.println(fish);
        fish.respond();
        fish.eat();

        System.out.println("-----------------------");
        System.out.println("Below information about a domestic cat");

        System.out.println(dmCat);
        dmCat.respond();
        dmCat.eat();
        dmCat.foul();

        System.out.println("-----------------------");
        System.out.println("Below information about a robo-cat");

        System.out.println(rbCat);
        rbCat.respond();
        rbCat.eat();
        rbCat.foul();

        System.out.println("-----------------------");
        System.out.println("Below information about a tiranosaur");

        System.out.println(tRex);
        tRex.respond();
        tRex.eat();

        System.out.println("-----------------------");
        System.out.println("Below information about a man");

        System.out.println(man);
        man.greetPet();
        man.describePet();
        man.repairCar();

        System.out.println("-----------------------");
        System.out.println("Below information about a woman");

        System.out.println(woman);
        woman.greetPet();
        woman.describePet();
        woman.makeup();

        System.out.println("-----------------------");
        System.out.println("Below information about a child");

        System.out.println(child);
        child.greetPet();
        child.describePet();
    }
}