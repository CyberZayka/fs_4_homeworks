package family_project.animals;

import family_project.Species;

public class Dog extends Pet {
    public Dog(String nickname, int age, String[] habits) {
        super(nickname, age, 0, habits);
        super.setSpecies(Species.DOG);
    }

    @Override
    public void respond() {
        System.out.println("Привіт, господар! Я за тобою скучив");
    }
}
