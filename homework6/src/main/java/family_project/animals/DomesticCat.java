package family_project.animals;

import family_project.Species;

public class DomesticCat extends Pet implements IPet {
    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        super.setSpecies(Species.DOMESTICCAT);
    }
    @Override
    public void foul() {
        System.out.println("Треба замісти сліди...");
    }
    @Override
    public void respond() {
        System.out.println("почухай пузіко");
    }
}
