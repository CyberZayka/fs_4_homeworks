package family_project.animals;

public class Tiranosaur extends Pet {
    public Tiranosaur(String nickname, int age) {
        super(nickname, age);
    }
    @Override
    public void respond() {
        System.out.println("Рррррр");
    }
}
