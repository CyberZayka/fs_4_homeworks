package family_project.animals;

import family_project.Species;

public class Fish extends Pet {
    public Fish(String nickname, int age) {
        super(nickname, age, 0, new String[]{});
        super.setSpecies(Species.FISH);
    }

    @Override
    public void respond() {
        System.out.println("буль-буль...");
    }
}
