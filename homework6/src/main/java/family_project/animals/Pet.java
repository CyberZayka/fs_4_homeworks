package family_project.animals;

import family_project.Species;

import java.util.Arrays;

public abstract class Pet {
    private Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public Pet(String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet(String nickname, int age) {
        this(nickname, age, 0, new String[]{});
    }

    public Pet(String nickname) {
        this(nickname, 0, 0, new String[]{});
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public void eat() {
        System.out.println("Я їм");
    }
    public abstract void respond();

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(species.name());
        sb.append("{");
        sb.append("nickname='").append(nickname).append("' ");
        if(age != 0) {
            sb.append(", age=").append(age);
        }
        if(trickLevel != 0) {
            sb.append(", trickLevel=").append(trickLevel);
        }
        if(habits.length != 0) {
            sb.append(", habits=").append(Arrays.toString(habits));
        }
        sb.append("}");
        return sb.toString();
    }
    @Override
    protected void finalize() throws Throwable {
        try {
            System.out.printf("Instance %s will be deleted\n", this);
        } catch (Throwable ex) {
            throw ex;
        } finally {
            super.finalize();
            System.out.println("Instance was removed successfully\n");
        }
    }
}
