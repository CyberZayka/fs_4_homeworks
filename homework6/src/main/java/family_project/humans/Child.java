package family_project.humans;

import family_project.animals.Pet;

final public class Child extends Human {
    public Child(String name, String surname, int year, Pet pet) {
        super(name, surname, year, 0, new String[][]{}, pet);
    }
    @Override
    public void greetPet() { System.out.printf("%s, пішли гратися!\n", super.getPet().getNickname()); }
}
