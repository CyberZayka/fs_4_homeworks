package family_project.humans;

import family_project.animals.Pet;

final public class Man extends Human {
    public Man(String name, String surname, int year, int iq, String[][] schedule, Pet pet) {
        super(name, surname, year, iq, schedule, pet);
    }

    public void repairCar() {
        System.out.println("Машина заглохла, треба відремонтувати!");
    }

    @Override
    public void greetPet() {
        System.out.printf("Привіт, %s, пішли гуляти!\n", super.getPet().getNickname());
    }
}
