package family_project.humans;

import family_project.animals.Pet;

final public class Woman extends Human {
    public Woman(String name, String surname, int year, int iq, String[][] schedule, Pet pet) {
        super(name, surname, year, iq, schedule, pet);
    }

    public void makeup() {
        System.out.println("Треба підкрасити вії");
    }

    @Override
    public void greetPet() {
        System.out.printf("%s, хочеш їсти?\n", super.getPet().getNickname());
    }
}